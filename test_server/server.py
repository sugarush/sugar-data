import asyncio

from quart import Quart
from sugar_api import CORS
from sugar_concache.mongo import MongoDB
from sugar_concache.redis import RedisDB


CORS.set_origins('*')

server = Quart('test')

RedisDB.defaults = {
    'host': 'redis://localhost'
}

@server.before_serving
async def setup_database():
    loop = asyncio.get_event_loop()
    MongoDB.set_event_loop(loop)
    await RedisDB.set_event_loop(loop)

from server import server

from authentication import Authentication

from models.user import User


server.register_blueprint(Authentication.resource(url_prefix='/v1'))

server.register_blueprint(User.resource(url_prefix='/v1', events=True))

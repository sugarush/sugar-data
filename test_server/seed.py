from sugar_odm import MongoDB

from server import server

from models.user import User


@server.before_serving
async def seed_superuser():
    user = await User.find_one({ 'username': 'admin' })

    if not user:
        user = await User.add({
            'username': 'admin',
            'password': 'admin',
            'groups': [ 'administrator' ]
        })

    print('Administrator ID: ' + user.id)
